#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "iostream"
#include "string"

// déclaration de variables globales
int coup = 0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->message->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}

// Traitement à réaliser lorsqu'on clique sur le bouton Jouer
void MainWindow::on_BtnJouer_clicked()
{
    // déclaration de constantes
     const int max = 6, min = 1;    // pour fixer la valeur minimale et la valeur maximale dun dé

    // déclaration de variables
    int nbUn, nbDeux, nbTrois;     // stockage des valeurs générées aléatoirement

    // exemple de la syntaxe à suivre pour placer une image dans le 1er label se nommant image1
    ui->image1->setPixmap(QPixmap(":/images/smiley.png"));

    // exemple de la syntaxe à suivre pour placer une image dans le 2ème label se nommant image2
     ui->image2->setPixmap(QPixmap(":/images/smiley.png"));

     // exemple de la syntaxe à suivre pour placer une image dans le 3ème label se nommant image3
     ui->image3->setPixmap(QPixmap(":/images/smiley.png"));

     // exemple de la syntaxe à suivre pour mettre du texte dans un label (ici le label nommé message)
     ui->message->setText("Code - A Compléter !");
     // sytaxe pour modifier la couleur du texte du label
     ui->message->setStyleSheet("color:orange");
     // syntaxe pour rendre visible le label  ("caché" avec hide() précédemment)
     ui->message->show();

    /* TO DO 1-1 ==> générer un premier nombre aléatoire à affecter à la variable nbUn  */


    /* TO DO 1-2 ==> en fonction de la valeur de nbUn, affichage de la face de dé correspondante dans le QLabel image1  */


    /* TO DO 2-1 ==> générer un deuxième nombre aléatoire à affecter à la variable nbDeux  */


    /* TO DO 2-2 ==> en fonction de la valeur de nbDeux, affichage de la face de dé correspondante dans le QLabel image2  */


    /* TO DO 3-1 ==> générer un troisième nombre aléatoire à affecter à la variable nbTrois  */


    /* TO DO 3-2 ==> en fonction de la valeur de nbTrois, affichage de la face de dé correspondante dans le QLabel image3  */



     // affichage du QLabel message (pour afficher Perdu ou Gagné)
     ui->message->show();
     // variables stockant le message qui sera à affecter au QLabel message en fonction du tir gagnant ou perdant
     QString message1 = "Gagné !!";
     QString message2 = "Perdu !";

    // TO DO 4 ==> tests des valeurs ==> en fonction des valeurs des 3 nombres générés précédemment, on a gagné si ...
    // if (// à compléter ... )


    // else // sinon, on a perdu !





}
